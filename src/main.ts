import {createApp} from 'vue'

import App from '@/App.vue'
import router from '@/router'

import 'bootstrap'
import 'bootstrap/scss/bootstrap.scss'
import '@/styles/style.scss'
import '@/styles/style.css'

import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {
    faAngleDown,
    faAngleRight,
    faArrowRight,
    faCalendarDay,
    faCheck,
    faEnvelope,
    faGenderless,
    faGlobe,
    faLink,
    faMapMarker,
    faPhone,
    faPlus,
    faSync,
    faTimes
} from '@fortawesome/free-solid-svg-icons';

library.add(faArrowRight, faAngleDown, faAngleRight, faMapMarker, faPhone, faEnvelope, faLink, faGenderless, faCalendarDay, faGlobe, faCheck, faTimes, faPlus, faSync);

createApp(App)
    .component('font-awesome-icon', FontAwesomeIcon)
    .use(router)
    .mount('#app')

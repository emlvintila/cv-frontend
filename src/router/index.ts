import {createRouter, createWebHashHistory, RouteRecordRaw} from 'vue-router'
import EditCv from "@/views/EditCv.vue";

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'Edit My CV',
        component: EditCv
    },
    {
        path: '/publications',
        name: 'View My Publications',
        component: () => import('../views/Publications.vue')
    },
    {
        path: '/print',
        name: 'Print My Cv',
        component: () => import('../views/PrintCv.vue')
    },
    {
        path: '/import',
        name: 'Import',
        component: () => import('../views/Import.vue')
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router

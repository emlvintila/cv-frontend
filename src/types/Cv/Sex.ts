export enum Sex {
    Unspecified = '',
    M = 'M',
    F = 'F'
}

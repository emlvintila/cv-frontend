import {CreatorTypes, Fields, ItemTypes} from "@/types/SchemaType";

export interface CreatorInstance {
    type: keyof CreatorTypes,
    value: string
}

export interface CvItemExtra {
    guid: string,
    univ: {
        cnatdcu: {
            $dirty: boolean,
            id: number,
            type: string
        }
    }
    itemType: {
        type: keyof ItemTypes
    };
    creators: CreatorInstance[],
    fields: Partial<{
        [key in keyof Fields]: {
            value: string
        }
    }>,
    tags: string[],
    related: string[]
}

export interface CvItemExtraMeta {
    itemType: {
        name: string
    },
    creators: Partial<{
        [key in keyof CreatorTypes]: {
            name: string
        }
    }>,
    fields: Partial<{
        [key in keyof Fields]: {
            name: string,
            type?: string,
            autocomplete?: string,
            pattern: string
        }
    }>
}

export type CvSectionExtra = CvItemExtra[]

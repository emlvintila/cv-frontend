export interface CvItemEducation {
    school: string,
    degree: string
    start: string,
    end: string
}

export type CvSectionEducation = CvItemEducation[]

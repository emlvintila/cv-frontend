import {Sex} from "@/types/Cv/Sex";

export interface CvSectionPersonalInfo {
    name: string,
    address: string,
    phone: string,
    email: string,
    website: string,
    sex: Sex,
    birthday: string,
    nationality: string
}

export interface CvItemExperience {
    company: string,
    position: string,
    start: string,
    end: string
}

export type CvSectionExperience = CvItemExperience[]

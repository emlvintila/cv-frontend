export interface CvItemSkill {
    skill: string
}

export type CvSectionSkills = CvItemSkill[]

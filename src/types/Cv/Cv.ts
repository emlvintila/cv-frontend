import {CvSectionPersonalInfo} from "@/types/Cv/Sections/CvSectionPersonalInfo";
import {CvSectionExperience} from "@/types/Cv/Sections/CvSectionExperience";
import {CvSectionEducation} from "@/types/Cv/Sections/CvSectionEducation";
import {CvSectionSkills} from "@/types/Cv/Sections/CvSectionSkills";
import {CvSectionExtra} from "@/types/Cv/Sections/CvSectionExtra";

export interface Cv {
    sections: {
        personalInfo: CvSectionPersonalInfo,
        experience: CvSectionExperience,
        education: CvSectionEducation,
        skills: CvSectionSkills,
        extra: CvSectionExtra
    }
}

export async function sleep(ms: number): Promise<void> {
    return new Promise<void>(resolve => setTimeout(resolve, ms));
}

export async function fetch2(input: Request | string, init?: RequestInit | undefined, options?: { timeout: number } | undefined): Promise<Response> {
    const {timeout = 10000} = options ?? {};
    const controller = new AbortController();
    const id = setTimeout(() => controller.abort, timeout);

    const response = await fetch(input, {
        ...init,
        signal: controller.signal
    });
    clearTimeout(id);

    return response;
}

export function firstNonBlank(...strings: string[]): string | undefined {
    return strings.find(str => str.trim().length > 0);
}

export function id<T>(x: T): T {
    return x;
}

export function htmlEntities(xml: string): string {
    const p = document.createElement("p");
    p.textContent = xml;

    return p.innerHTML;
}

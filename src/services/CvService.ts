import {Cv} from "@/types/Cv/Cv";
import {CvItemExperience} from "@/types/Cv/Sections/CvSectionExperience";
import {CvItemEducation} from "@/types/Cv/Sections/CvSectionEducation";
import {CvItemSkill} from "@/types/Cv/Sections/CvSectionSkills";
import {Fields, ItemTypes} from "@/types/SchemaType";
import {CreatorInstance, CvItemExtra} from "@/types/Cv/Sections/CvSectionExtra";
import {Sex} from "@/types/Cv/Sex";
import {fetch2} from "@/utils/utils";
import * as uuid from "uuid/index";
import Constants from "@/Constants";

export default class CvService {
    private static BASE_URL = `${Constants.API_BASE_URL}/api/cv`;
    static KEY = 'Cv';

    static getEmptyCv(): Cv {
        return {
            sections: {
                personalInfo: {
                    name: '',
                    address: '',
                    phone: '',
                    email: '',
                    sex: Sex.Unspecified,
                    website: '',
                    birthday: '',
                    nationality: ''
                },
                education: [],
                experience: [],
                skills: [],
                extra: []
            }
        }
    }

    async save(Cv: Cv) {
        //localStorage.setItem(CvService.KEY, JSON.stringify(Cv));
        await fetch2(`${CvService.BASE_URL}/my`, {
            mode: 'cors',
            method: 'post',
            body: JSON.stringify(Cv)
        }, {timeout: 10000});
    }

    async load(): Promise<Cv> {
        //const json = localStorage.getItem(CvService.KEY);
        const response = await fetch2(`${CvService.BASE_URL}/my`, {
            mode: 'cors',
            method: 'get',
        }, {timeout: 10000});

        if (response.ok)
            return await response.json() as Cv;

        return CvService.getEmptyCv();
    }

    addNewEducation(cv: Cv): CvItemEducation {
        const edu: CvItemEducation = {
            school: '',
            degree: '',
            start: '',
            end: ''
        };
        cv.sections.education.push(edu)

        return edu;
    }

    addNewExperience(cv: Cv): CvItemExperience {
        const exp: CvItemExperience = {
            company: '',
            position: '',
            start: '',
            end: ''
        };
        cv.sections.experience.push(exp);

        return exp;
    }

    addNewExtra(cv: Cv, type: keyof ItemTypes, fields: (keyof Fields)[], creators: CreatorInstance[]): CvItemExtra {
        if (fields.length === 0)
            throw new Error(`No fields found for type ${type}`);

        const extra: CvItemExtra = {
            guid: uuid.v4(),
            univ: {
                cnatdcu: {
                    $dirty: true,
                    id: 0,
                    type: "" // todo: fix type
                }
            },
            itemType: {
                type: type
            },
            fields: fields.reduce((obj: object, field: keyof Fields) => ({
                ...obj,
                [field]: {
                    value: ''
                }
            }), Object.create(null)),
            creators,
            tags: [],
            related: []
        }
        cv.sections.extra.push(extra);

        return extra;
    }

    addNewSkill(cv: Cv): CvItemSkill {
        const sk: CvItemSkill = {skill: ''};
        cv.sections.skills.push(sk);

        return sk;
    }

    removeEducation(cv: Cv, idx: number): void {
        cv.sections.education.splice(idx, 1);
    }

    removeExperience(cv: Cv, idx: number): void {
        cv.sections.experience.splice(idx, 1);
    }

    removeExtra(cv: Cv, idx: number | CvItemExtra): void {
        const start = typeof idx === 'number' ?
            idx :
            cv.sections.extra.findIndex(i => i === idx);
        cv.sections.extra.splice(start, 1);
    }

    removeSkill(cv: Cv, idx: number): void {
        cv.sections.skills.splice(idx, 1);
    }
}

import {SchemaType} from "@/types/SchemaType";
import {fetch2} from "@/utils/utils";
import Constants from "@/Constants";

export default class SchemaService {
    private static CACHE_KEYS = {
        schema: 'Schema.Schema',
        version: 'Schema.Version',
    };

    private static BASE_URL = `${Constants.API_BASE_URL}/api/schema`;

    static getEmptySchema(): SchemaType {
        return {
            version: 0,
            itemTypes: [],
            meta: {
                fields: {}
            },
            locales: {}
        };
    }

    public async ensureSchemaUpToDate(): Promise<SchemaType> {
        if (await this.isSchemaUpToDate())
            return this.getSchemaCached()!;

        return await this.updateSchemaFromServer();
    }

    public isSchemaCached(): boolean {
        return typeof localStorage[SchemaService.CACHE_KEYS.schema] !== 'undefined' &&
            typeof localStorage[SchemaService.CACHE_KEYS.version] !== 'undefined';
    }

    public getSchemaCached(): SchemaType | null {
        const json = localStorage.getItem(SchemaService.CACHE_KEYS.schema);
        return json ? JSON.parse(json) : null;
    }

    public getSchemaVersionCached(): number | null {
        const ver = localStorage.getItem(SchemaService.CACHE_KEYS.version);
        return ver ? Number.parseInt(ver) : null;
    }

    public async isSchemaUpToDate(): Promise<boolean> {
        if (!this.isSchemaCached())
            return false;

        return await this.getSchemaVersionFromServer() === this.getSchemaVersionCached()!;
    }

    public async getSchemaVersionFromServer(): Promise<number> {
        const response = await fetch2(`${SchemaService.BASE_URL}/version`);

        return Number.parseInt(await response.text());
    }

    public async updateSchemaFromServer(): Promise<SchemaType> {
        const response = await fetch2(SchemaService.BASE_URL);
        const schema = await response.json() as SchemaType;

        this.cacheSchema(schema);
        return schema;
    }

    // noinspection JSMethodCanBeStatic
    private cacheSchema(schema: SchemaType) {
        localStorage.setItem(SchemaService.CACHE_KEYS.version, schema.version.toString());
        localStorage.setItem(SchemaService.CACHE_KEYS.schema, JSON.stringify(schema));
    }
}

import {CvItemExtra} from "@/types/Cv/Sections/CvSectionExtra";
import {Cv} from "@/types/Cv/Cv";

export default class CvUtils {
    public static getItemForGuid(cv: Cv, guid: string): CvItemExtra | undefined {
        return cv.sections.extra.find(item => item.guid === guid);
    }

    public static getNameForCvItemExtra(item: CvItemExtra): string | undefined {
        const fields = item.fields;
        return fields?.title?.value ??
            fields?.blogTitle?.value ??
            fields?.bookTitle?.value ??
            fields?.dictionaryTitle?.value ??
            fields?.encyclopediaTitle?.value ??
            fields?.forumTitle?.value ??
            fields?.proceedingsTitle?.value ??
            fields?.programTitle?.value ??
            fields?.publicationTitle?.value ??
            fields?.seriesTitle?.value ??
            fields?.shortTitle?.value ??
            fields?.websiteTitle?.value;
    }

    public static getNameForGuid(cv: Cv, guid: string): string {
        const item = this.getItemForGuid(cv, guid);
        return item ? this.getNameForCvItemExtra(item) ?? guid : guid;
    }
}

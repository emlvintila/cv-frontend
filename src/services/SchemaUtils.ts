import {CreatorTypes, Fields, ItemTypes, SchemaType} from "@/types/SchemaType";
import {CvItemExtraMeta} from "@/types/Cv/Sections/CvSectionExtra";

export default class SchemaUtils {
    static getMetaForItemType(schema: SchemaType, itemType: keyof ItemTypes): CvItemExtraMeta {
        const fields = this.getFieldsForItemType(schema, itemType)
            .reduce((obj: object, field) => {
                return {
                    ...obj,
                    [field]: {
                        name: this.getNameForField(schema, field),
                        type: this.getTypeForField(schema, field),
                        autocomplete: this.getAutocompleteForField(schema, field),
                        pattern: this.getPatternForField(schema, field)
                    }
                }
            }, Object.create(null));
        const creators = this.getCreatorTypesForItemType(schema, itemType)
            .reduce((obj: object, creator) => {
                return {
                    ...obj,
                    [creator]: {
                        name: this.getNameForCreatorType(schema, creator)
                    }
                }
            }, Object.create(null));

        return {
            itemType: {
                name: this.getNameForItemType(schema, itemType)
            },
            fields: fields,
            creators: creators,
        }
    }

    static getNameForItemType(schema: SchemaType, itemType: keyof ItemTypes): string {
        const locale = 'ro-RO';
        const translations = schema.locales[locale];

        return translations.itemTypes[itemType];
    }

    static getNameForField(schema: SchemaType, field: keyof Fields): string {
        const locale = 'ro-RO'
        const translations = schema.locales[locale];

        return translations.fields[field];
    }

    static getNameForCreatorType(schema: SchemaType, creatorType: keyof CreatorTypes): string {
        const locale = 'ro-RO';
        const translations = schema.locales[locale];

        return translations.creatorTypes[creatorType];
    }

    static getFieldsForItemType(schema: SchemaType, itemType: keyof ItemTypes): (keyof Fields)[] {
        return schema
            .itemTypes
            .find(i => i.itemType === itemType)
            ?.fields
            .map(f => f.field) ?? [];
    }

    static getCreatorTypesForItemType(schema: SchemaType, itemType: keyof ItemTypes): (keyof CreatorTypes)[] {
        return schema
            .itemTypes
            .find(i => i.itemType === itemType)
            ?.creatorTypes
            .map(c => c.creatorType) ?? [];
    }

    static getTypeForField(schema: SchemaType, field: keyof Fields): string | undefined {
        return schema.meta.fields[field]?.type;
    }

    static getAllFieldsKeys(): (keyof Fields)[] {
        return Object.keys(new FieldsClass()) as (keyof Fields)[];
    }

    static getAutocompleteForField(schema: SchemaType, field: keyof Fields): string | undefined {
        return schema.meta.fields[field]?.autocomplete;
    }

    static getPatternForField(schema: SchemaType, field: keyof Fields): string | undefined {
        return schema.meta.fields[field]?.pattern;
    }
}

class FieldsClass implements Fields {
    DOI: string = "";
    ISBN: string = "";
    ISSN: string = "";
    abstractNote: string = "";
    accessDate: string = "";
    applicationNumber: string = "";
    archive: string = "";
    archiveLocation: string = "";
    artworkMedium: string = "";
    artworkSize: string = "";
    assignee: string = "";
    audioFileType: string = "";
    audioRecordingFormat: string = "";
    billNumber: string = "";
    blogTitle: string = "";
    bookTitle: string = "";
    callNumber: string = "";
    caseName: string = "";
    code: string = "";
    codeNumber: string = "";
    codePages: string = "";
    codeVolume: string = "";
    committee: string = "";
    company: string = "";
    conferenceName: string = "";
    country: string = "";
    court: string = "";
    date: string = "";
    dateAdded: string = "";
    dateDecided: string = "";
    dateEnacted: string = "";
    dateModified: string = "";
    dictionaryTitle: string = "";
    distributor: string = "";
    docketNumber: string = "";
    documentNumber: string = "";
    edition: string = "";
    encyclopediaTitle: string = "";
    episodeNumber: string = "";
    extra: string = "";
    filingDate: string = "";
    firstPage: string = "";
    forumTitle: string = "";
    genre: string = "";
    history: string = "";
    institution: string = "";
    interviewMedium: string = "";
    issue: string = "";
    issueDate: string = "";
    issuingAuthority: string = "";
    itemType: string = "";
    journalAbbreviation: string = "";
    label: string = "";
    language: string = "";
    legalStatus: string = "";
    legislativeBody: string = "";
    letterType: string = "";
    libraryCatalog: string = "";
    manuscriptType: string = "";
    mapType: string = "";
    meetingName: string = "";
    nameOfAct: string = "";
    network: string = "";
    numPages: string = "";
    number: string = "";
    numberOfVolumes: string = "";
    pages: string = "";
    patentNumber: string = "";
    place: string = "";
    postType: string = "";
    presentationType: string = "";
    priorityNumbers: string = "";
    proceedingsTitle: string = "";
    programTitle: string = "";
    programmingLanguage: string = "";
    publicLawNumber: string = "";
    publicationTitle: string = "";
    publisher: string = "";
    references: string = "";
    reportNumber: string = "";
    reportType: string = "";
    reporter: string = "";
    reporterVolume: string = "";
    rights: string = "";
    runningTime: string = "";
    scale: string = "";
    section: string = "";
    series: string = "";
    seriesNumber: string = "";
    seriesText: string = "";
    seriesTitle: string = "";
    session: string = "";
    shortTitle: string = "";
    studio: string = "";
    subject: string = "";
    system: string = "";
    thesisType: string = "";
    title: string = "";
    university: string = "";
    url: string = "";
    versionNumber: string = "";
    videoRecordingFormat: string = "";
    volume: string = "";
    websiteTitle: string = "";
    websiteType: string = "";
}

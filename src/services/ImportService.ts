import {fetch2, htmlEntities, id} from "@/utils/utils";
import Constants from "@/Constants";
import {Fields, ItemTypes, SchemaType} from "@/types/SchemaType";
import {CvItemExtra} from "@/types/Cv/Sections/CvSectionExtra";
import * as uuid from "uuid";
import SchemaUtils from "@/services/SchemaUtils";
import _merge from "lodash.merge";

export default class ImportService {
    private static BASE_URL = `${Constants.API_BASE_URL}/api/import`;
    private static DOMParser = new DOMParser();

    public async postImport(schema: SchemaType, filteredCriteriaTypes: { [k: string]: boolean }): Promise<CvItemExtra[]> {
        const response = await fetch2(`${ImportService.BASE_URL}/`, {
            mode: 'cors',
            method: 'post'
        }, {timeout: 10000});

        if (response.ok) {
            const obj = await response.json() as { id: number, values: string, type: string }[];
            return obj
                .map(item => (
                    {
                        id: item.id,
                        type: item.type,
                        xml: ImportService.DOMParser.parseFromString(
                            ImportService.DOMParser.parseFromString(item.values, "text/html")
                                .documentElement.querySelector("body")!.innerText, "text/html"
                        ).documentElement.querySelector("root")!,
                    }))
                .filter(i => i.type in ImportService.CnatdcuToCvMappings)
                .filter(i => filteredCriteriaTypes[i.type])
                .map(i => {
                    const item = {
                        guid: uuid.v4(),
                        univ: {
                            cnatdcu: {
                                $dirty: false,
                                id: i.id,
                                type: i.type
                            }
                        },
                        itemType: {type: ImportService.CnatdcuToCvMappings[i.type].itemType},
                        fields: SchemaUtils.getFieldsForItemType(schema, ImportService.CnatdcuToCvMappings[i.type].itemType)
                            .map(key => ({[key]: {value: ''}}))
                            .reduce(_merge),
                        related: [],
                        creators: [],
                        tags: []
                    } as CvItemExtra;
                    item.fields = {
                        ...item.fields,
                        ...Object.keys(ImportService.CnatdcuToCvMappings[i.type].fields)
                            .map(key => {
                                const field = ImportService.CnatdcuToCvMappings[i.type].fields[key];
                                const mappingFn = field.mapping ?? id;
                                const fieldValue = i.xml.querySelector(`field#${key}`)!.getAttribute("value")!;

                                return {
                                    [field.targetField]: {
                                        value: mappingFn(fieldValue, item, schema)
                                    }
                                };
                            })
                            .reduce(_merge)
                    };

                    const transformers = ImportService.CnatdcuToCvMappings[i.type].transformers;
                    if (transformers) {
                        Object.keys(transformers).forEach(key => {
                            const transformer = transformers[key];
                            const fieldValue = i.xml.querySelector(`field#${key}`)!.getAttribute("value")!;
                            transformer(fieldValue, item, schema);
                        });
                    }

                    return item;
                });
        } else {
            throw await response.text();
        }
    }

    public async getCriteriaDescriptions(): Promise<{ [k: string]: string[] }> {
        const response = await fetch2(`${ImportService.BASE_URL}/descriptions`, {
            mode: 'cors',
            method: 'get'
        }, {timeout: 10000});

        if (response.ok) {
            const obj = await response.json() as { type: number | string, description: string }[];
            return obj
                .filter(x => x.type !== null)
                .reduce((x, y) => {
                    const type = y.type.toString().trim();
                    return ({
                        ...x,
                        [type]: [...(x[type] ?? []), y.description]
                    });
                }, {} as { [k: string]: string[] });
        } else {
            throw await response.text();
        }
    }

    public async patchSync(items: CvItemExtra[]) {
        if (items.some(item => !item.univ.cnatdcu.$dirty)) {
            throw "all items passed must be $dirty";
        }

        const itemsToBePatched = items
            .filter(item => !!ImportService.CvToCnatdcuMappings[item.itemType.type]?.[item.univ.cnatdcu.type])
            .map(item => {
                const mapping = ImportService.CvToCnatdcuMappings[item.itemType.type]![item.univ.cnatdcu.type];
                const fields = (Object.keys(mapping.fields) as (keyof Fields)[]).map(key => {
                    const field = mapping.fields[key];
                    return field ? `<field id="${field.targetField}" value="${(field.mapping ?? id)(item.fields[key]!.value, item)}" />` : "";
                });
                if (mapping.computedFields) {
                    const computedFields = Object.keys(mapping.computedFields).map(key => {
                        const supplier = mapping.computedFields![key].supplier;
                        return `<field id="${key}" value="${supplier(item)}" />`
                    });
                    fields.push(...computedFields);
                }

                const fieldsXml = fields.join("");
                const xml = htmlEntities(`<root>${fieldsXml}</root>`);
                return ({
                    "ID_EvalUnivCriteriuProfesor": item.univ.cnatdcu.id,
                    "ValoriConfigurareCampuriCriteriu": xml
                });
            });

        return await fetch2(`${ImportService.BASE_URL}/sync`, {
            mode: 'cors',
            method: 'patch',
            body: JSON.stringify(itemsToBePatched)
        }, {timeout: 10000});
    }

    public static readonly CnatdcuToCvMappings: {
        [criteriaType: string]: CnatdcuToCvMapping
    } = Object.freeze({
        "A1.1": {
            itemType: "book",
            fields: {
                "titluC": {targetField: "title"},
                "link": {targetField: "url"},
                "editura": {targetField: "publisher"},
                "isbn": {targetField: "ISBN"},
                "AnAparitie": {targetField: "date", mapping: dateYearToDateStringMapping},
                "TotalNrPagini": {targetField: "numPages"}
            }
        },
        "3": {
            itemType: "book",
            fields: {
                "titluCc": {targetField: "title"},
                "link": {targetField: "url"},
                "editura": {targetField: "publisher"},
                "isbn": {targetField: "ISBN"},
                "AnAparitie": {targetField: "date", mapping: dateYearToDateStringMapping},
                "TotalNrPagini": {targetField: "numPages"}
            },
            transformers: {
                "NrAutori": transformerNrAutori
            }
        },
        "5": {
            itemType: "book",
            fields: {
                "titluC": {targetField: "title"},
                "link": {targetField: "url"},
                "editura": {targetField: "publisher"},
                "isbn": {targetField: "ISBN"},
                "AnAparitie": {targetField: "date", mapping: dateYearToDateStringMapping},
                "TotalNrPagini": {targetField: "numPages"}
            }
        },
        "15": {
            itemType: "patent",
            fields: {
                "Titlul": {targetField: "title"},
                "numar": {targetField: "patentNumber"},
                "AnAparitie": {targetField: "issueDate", mapping: dateYearToDateStringMapping}
            },
            transformers: {
                "autori": transformerAuthorList
            }
        },
        "17": {
            itemType: "computerProgram",
            fields: {
                "Titlul": {targetField: "title"},
                "link": {targetField: "url"},
                "AnAparitie": {targetField: "date", mapping: dateYearToDateStringMapping}
            }
        },
    });

    public static readonly CvToCnatdcuMappings: Partial<{
        [k in keyof ItemTypes]: {
            [k: string]: CvToCnatdcuMapping
        }
    }> = Object.freeze({
        "book": {
            "A1.1": {
                fields: {
                    "title": {targetField: "titluC"},
                    "url": {targetField: "link"},
                    "publisher": {targetField: "editura"},
                    "ISBN": {targetField: "isbn"},
                    "date": {targetField: "AnAparitie", mapping: dateStringToDateYearMapping},
                    "numPages": {targetField: "TotalNrPagini"}
                }
            },
            "3": {
                fields: {
                    "title": {targetField: "titluCc"},
                    "url": {targetField: "link"},
                    "publisher": {targetField: "editura"},
                    "ISBN": {targetField: "isbn"},
                    "date": {targetField: "AnAparitie", mapping: dateStringToDateYearMapping},
                    "numPages": {targetField: "TotalNrPagini"}
                }
            },
            "5": {
                fields: {
                    "title": {targetField: "titluC"},
                    "url": {targetField: "link"},
                    "publisher": {targetField: "editura"},
                    "ISBN": {targetField: "isbn"},
                    "date": {targetField: "AnAparitie", mapping: dateStringToDateYearMapping},
                    "numPages": {targetField: "TotalNrPagini"}
                }
            }
        },
        "patent": {
            "15": {
                fields: {
                    "title": {targetField: "Titlul"},
                    "patentNumber": {targetField: "numar"},
                    "issueDate": {targetField: "AnAparitie", mapping: dateStringToDateYearMapping},
                    "shortTitle": {targetField: "TitluScurt"}
                },
                computedFields: {
                    "autori": {
                        supplier: creatorsToCreatorListSupplier
                    }
                }
            }
        },
        "computerProgram": {
            "17": {
                fields: {
                    "title": {targetField: "Titlul"},
                    "url": {targetField: "link"},
                    "date": {targetField: "AnAparitie", mapping: dateStringToDateYearMapping}
                }
            }
        }
    });
}

function dateYearToDateStringMapping(year: string): string {
    return `${year}-01-01`;
}

function dateStringToDateYearMapping(date: string): string {
    try {
        return new Date(date).getFullYear().toString();
    } catch {
        return date.split("-")[0] ?? new Date(Date.now()).getFullYear().toString();
    }
}

function transformerNrAutori(fieldValue: string, item: CvItemExtra, schema: SchemaType): void {
    const creatorType = SchemaUtils.getCreatorTypesForItemType(schema, item.itemType.type)[0];
    const n = Number.parseInt(fieldValue);
    while (n < item.creators.length) {
        item.creators.push({
            type: creatorType,
            value: ''
        });
    }
}

function transformerAuthorList(fieldValue: string, item: CvItemExtra, schema: SchemaType): void {
    const creatorType = SchemaUtils.getCreatorTypesForItemType(schema, item.itemType.type)[0];
    item.creators.splice(0, item.creators.length,
        ...fieldValue
            .split(",")
            .map(author => ({
                type: creatorType,
                value: author.trim()
            }))
    );
}

function creatorsToCreatorListSupplier(item: CvItemExtra): string {
    return item.creators.map(creator => creator.value.trim()).join(", ")
}

type CnatdcuToCvMappingFn = (fieldValue: string, item: CvItemExtra, schema: SchemaType) => string;
type CnatdcuToCvMappingTransformer = (fieldValue: string, item: CvItemExtra, schema: SchemaType) => void;

export interface CnatdcuToCvMapping {
    itemType: keyof ItemTypes,
    fields: {
        [k: string]: {
            targetField: keyof Fields,
            mapping?: CnatdcuToCvMappingFn
        }
    },
    transformers?: {
        [k: string]: CnatdcuToCvMappingTransformer
    }
}

type CvToCnatdcuMappingFn = (fieldValue: string, item: CvItemExtra) => string;
type CvToCnatdcuFieldSupplier = (item: CvItemExtra) => string;

export interface CvToCnatdcuMapping {
    fields: Partial<{
        [k in keyof Fields]: {
            targetField: string,
            mapping?: CvToCnatdcuMappingFn
        }
    }>,
    computedFields?: {
        [k: string]: {
            supplier: CvToCnatdcuFieldSupplier
        }
    }
}
